var kb__gpio_8h =
[
    [ "GPIO_FIRST", "kb__gpio_8h.html#a030c92651d0a15e98e186fe0cdd27772", null ],
    [ "GPIO_INPUT", "kb__gpio_8h.html#a7be6a0cc9aa65da1d4ee5751b4085853", null ],
    [ "GPIO_KH4_RESET", "kb__gpio_8h.html#a2b16735d4be2029a76c1813c2a9c1b73", null ],
    [ "GPIO_LAST", "kb__gpio_8h.html#ae27ba6a1ba59a96c6064d61535502329", null ],
    [ "GPIO_OUTPUT", "kb__gpio_8h.html#a0db9fe8a278e6ab7c5c6f14fe58e5eb1", null ],
    [ "GPIO_SMUTE", "kb__gpio_8h.html#a729278d8ad52993fe7c0e288e68ec856", null ],
    [ "kb_gpio_cleanup", "kb__gpio_8h.html#ae954471145cc6585763aeb01842d61c6", null ],
    [ "kb_gpio_clear", "kb__gpio_8h.html#a4d046c0445b753de64ffb38888ac4bfd", null ],
    [ "kb_gpio_dir", "kb__gpio_8h.html#a1cd4956c014dd1f950f060357edcbe58", null ],
    [ "kb_gpio_dir_val", "kb__gpio_8h.html#af94738e22e2a2a2af6b3a618d94fd4a4", null ],
    [ "kb_gpio_function", "kb__gpio_8h.html#aa35b4abbeed41042307344df3d9fb498", null ],
    [ "kb_gpio_get", "kb__gpio_8h.html#ab27da43aa5cbdd9cc0e9ffdf89e343ae", null ],
    [ "kb_gpio_init", "kb__gpio_8h.html#a985bbe2660af7e41e96720eb8f9393b3", null ],
    [ "kb_gpio_set", "kb__gpio_8h.html#a9c15d4671a8c8e9b2cb25245dc878574", null ]
];