var structurg__state__t =
[
    [ "parameters_t", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebf", [
      [ "MODL", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfa50e4945291a5081e96215d2d4c644fb1", null ],
      [ "DMIN", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfaea17425f2a149dee87c443a787faf581", null ],
      [ "DMAX", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfae6bca46a6f035a29f2c4c6a428eeabe8", null ],
      [ "ARES", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfaa0db8673b27836ba21d23a02a8bf3d8f", null ],
      [ "AMIN", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfa1b308d048cf875111957794b6fa97274", null ],
      [ "AMAX", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfa6438aec6ae47a95dbae97bea61744283", null ],
      [ "AFRT", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfa68f7e1c3969dfbdddc08df635ecdca03", null ],
      [ "SCAN", "structurg__state__t.html#a9c95d0520efa55de4bfb2fc8e8c92ebfa78673635737078b8b50f91157608c8b2", null ]
    ] ],
    [ "area_front", "structurg__state__t.html#a43cfd5ce7a70e8f15de55ca188678791", null ],
    [ "area_max", "structurg__state__t.html#ac76c41ef8066f24e4c8e1758cd89b28e", null ],
    [ "area_min", "structurg__state__t.html#a6ea007499c3f0199c310515062609113", null ],
    [ "area_total", "structurg__state__t.html#ae1a02216a5cd0f5d1e26670f5c79cc6f", null ],
    [ "distance_max", "structurg__state__t.html#a52d009d425594843058eb952b649978f", null ],
    [ "distance_min", "structurg__state__t.html#a8393791dd4b200190a4473beb8eea671", null ],
    [ "first", "structurg__state__t.html#a5bb6b7e8ef73d9b7511775544e11d904", null ],
    [ "last", "structurg__state__t.html#ad3ff0440131381f7c187e8867b224627", null ],
    [ "last_timestamp", "structurg__state__t.html#a1fdf067ec52b4d63b423710932b25d42", null ],
    [ "max_size", "structurg__state__t.html#ad3b34aceaa938fdcaec4dc64dc7f157b", null ],
    [ "model", "structurg__state__t.html#a0ae9954f187f4ab9f0d5df360eee9894", null ],
    [ "parameter", "structurg__state__t.html#a4c3c534f72ce0cac9bc7be991015ad9e", null ],
    [ "scan_rpm", "structurg__state__t.html#aea2f85711e7f4ed5ee0abcca6f80e82a", null ]
];