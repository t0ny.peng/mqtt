var kb__error_8c =
[
    [ "kb_debug", "kb__error_8c.html#a910ded5bfba07e3b5e93202368155502", null ],
    [ "kb_error", "kb__error_8c.html#a9cb11fb40aba13d25f44b89cc71f5e67", null ],
    [ "kb_fatal", "kb__error_8c.html#a01fbf1c562330e074ec9331d98ecc756", null ],
    [ "kb_msg", "kb__error_8c.html#a4e87b68cbeda7d5b4f3f17db17edb320", null ],
    [ "kb_set_debug_level", "kb__error_8c.html#aa37d3cf5b9b4e8a961c0f7a405664cc6", null ],
    [ "kb_set_debug_mask", "kb__error_8c.html#ac6f87979a98520fdec98f9ff9d4c05b1", null ],
    [ "kb_set_error_handler", "kb__error_8c.html#a72bbee6ffcb1c8be063966d128fe3a0a", null ],
    [ "kb_vdebug", "kb__error_8c.html#a77911ee85bfd123298a2dfb8f3019e21", null ],
    [ "kb_verror", "kb__error_8c.html#a3384c94e5877279d11bed633638184d5", null ],
    [ "kb_vmsg", "kb__error_8c.html#aa621635a59c5b1d2190a0469d00ec263", null ],
    [ "kb_vwarning", "kb__error_8c.html#a6b59343a29eb6d19b3fd454186e6e5cd", null ],
    [ "kb_warning", "kb__error_8c.html#a0f83c793a43fabfda5685be6bc7615f2", null ],
    [ "kb_debug_level", "kb__error_8c.html#aa6599f99370378b5ac58057844abb592", null ],
    [ "kb_debug_mask", "kb__error_8c.html#a04ffdaed619e8c42aec29a55a6025ed4", null ],
    [ "kb_errmsg", "kb__error_8c.html#a1fcbc41d390920ded9400276de6d5701", null ],
    [ "kb_user_error_handler", "kb__error_8c.html#a6b3a3d2d738c7cc920623ea641682351", null ],
    [ "kb_user_warning_handler", "kb__error_8c.html#a18cc20d2735201a9d3d5eca25d824443", null ],
    [ "kb_warnmsg", "kb__error_8c.html#ac8ac8df59de483c37d79d5a94e958360", null ]
];