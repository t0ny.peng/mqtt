var koreio_8c =
[
    [ "kio_ChangeIO", "koreio_8c.html#aefbf593655fa14e6e5bf626afe95edc6", null ],
    [ "kio_ChangeLed", "koreio_8c.html#a0b68a727284f4b3b8b72211f21c2afe1", null ],
    [ "kio_ChangePW", "koreio_8c.html#ab243a3c40639257733ab90d0a22196ac", null ],
    [ "kio_ChangePWM_freq", "koreio_8c.html#a0769d5a8d2be0eb97523a3fed4dc90b7", null ],
    [ "kio_ChangePWM_ratio", "koreio_8c.html#ab6a06a4f584b5e2f954622be0add1025", null ],
    [ "kio_ClearIO", "koreio_8c.html#afa84586af458c590016d494355da876d", null ],
    [ "kio_ClearPW", "koreio_8c.html#a399378ec6c99817a649c34ef6dcc3923", null ],
    [ "kio_ConfigIO", "koreio_8c.html#a8de2871a0489627df03903de29ca5121", null ],
    [ "kio_GetFWVersion", "koreio_8c.html#a0ade8d7eca0e9afe2efeebe0e18b0d11", null ],
    [ "kio_i2c_ListScan", "koreio_8c.html#a8b4fa96d26d0ecf71dab72279bb890b5", null ],
    [ "kio_i2c_ReturnRead", "koreio_8c.html#aba7a089cc26ead2133e5a70d0bf6fc6a", null ],
    [ "kio_i2c_StartRead", "koreio_8c.html#ab0350a42088da06d3dfb1ce81a3ab70f", null ],
    [ "kio_i2c_StartScan", "koreio_8c.html#a0d90cb709426547b3afc689fc9e5a4b9", null ],
    [ "kio_i2c_Write", "koreio_8c.html#a52e25f40eaa87fde9351c1db824b4f61", null ],
    [ "kio_ReadAnalog", "koreio_8c.html#af41ec56e036ceca45ed262f28687e40a", null ],
    [ "kio_ReadCAN", "koreio_8c.html#ac25b794aa3b941afe0c92ba48e32be4b", null ],
    [ "kio_ReadIO", "koreio_8c.html#a728096153bd62def5557c25059d2b3ca", null ],
    [ "kio_SendCAN", "koreio_8c.html#a19d86c8e3b1eefe2e308c8d14be994b0", null ],
    [ "kio_SetANValue", "koreio_8c.html#ab57ed0ac78e87f47595b64cf7badf9eb", null ],
    [ "kio_SetIO", "koreio_8c.html#a4e96c3878ce5681cb6e7498da44ef169", null ],
    [ "kio_SetPW", "koreio_8c.html#ae4ce1583f43ed436fa10a1da7e5c359a", null ],
    [ "kio_Timer", "koreio_8c.html#a83fc782f0967143e3cd695d98ccf3f8b", null ]
];