var overo_pwm__ioctl_8h =
[
    [ "PWM_Value_t", "structPWM__Value__t.html", "structPWM__Value__t" ],
    [ "PWM_CMD_OFF", "overo-pwm__ioctl_8h.html#a1888dba8f28cd00c3cfd336e7216a2b0", null ],
    [ "PWM_CMD_ON", "overo-pwm__ioctl_8h.html#ab525bea1d9fd00b86858450aa10918c9", null ],
    [ "PWM_CMD_SET_DUTY", "overo-pwm__ioctl_8h.html#a7db3eb663bab4017724ae291a2f3cf82", null ],
    [ "PWM_CMD_SET_FREQUENCY", "overo-pwm__ioctl_8h.html#a27fa200f601bdceea572df32076aa9ba", null ],
    [ "PWM_IOC_MAGIC", "overo-pwm__ioctl_8h.html#a5899b5088eae3b25e9bd240285097af2", null ],
    [ "PWM_IOC_MAXNR", "overo-pwm__ioctl_8h.html#ad30e5810ab12e4dddf97447e85509e75", null ],
    [ "PWM_IOCTL_OFF", "overo-pwm__ioctl_8h.html#a2c241bd381028ef7636543f318fc1c71", null ],
    [ "PWM_IOCTL_ON", "overo-pwm__ioctl_8h.html#a7bdc31accd570ba29bbb25c538208531", null ],
    [ "PWM_IOCTL_SET_DUTY", "overo-pwm__ioctl_8h.html#ae2236a82a1da569b603759d3bf3c8cd1", null ],
    [ "PWM_IOCTL_SET_FREQUENCY", "overo-pwm__ioctl_8h.html#a16e90b90b41390333772b63a444fb9b4", null ]
];