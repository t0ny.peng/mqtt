var kb__socket_8c =
[
    [ "DieWithError", "kb__socket_8c.html#a2b538227cf83afa6aedb5735f26c25c7", null ],
    [ "ksock_add_command", "kb__socket_8c.html#aa7562cd86a7003ec86223762986cc286", null ],
    [ "ksock_connect", "kb__socket_8c.html#afa2f4ff49566a3dd3b8dee453649b734", null ],
    [ "ksock_exec_command", "kb__socket_8c.html#a04c7a4782c4ed34429bfa14b7c8bbd46", null ],
    [ "ksock_exec_command_pending", "kb__socket_8c.html#a50b1699aa79204ca924184ed0cae9eb6", null ],
    [ "ksock_get_command", "kb__socket_8c.html#abaadd2fe326d38bbb98a9521e1a29226", null ],
    [ "ksock_init", "kb__socket_8c.html#a27050edac9941f2cf5b7c3c52ba697fe", null ],
    [ "ksock_next_connection", "kb__socket_8c.html#a2850ce8f7c3e0956de8e706fed1a5922", null ],
    [ "ksock_remove_command", "kb__socket_8c.html#a49808430918ebed463f9d3e91ec04123", null ],
    [ "ksock_send_answer", "kb__socket_8c.html#a5c3bae414ded845ac60780a2a6118691", null ],
    [ "ksock_send_command", "kb__socket_8c.html#a7e3dd402b1d8bf3c03bdf09f1994cf9b", null ],
    [ "ksock_server_close", "kb__socket_8c.html#aa00a3ae29cece61381c9a5140722f654", null ],
    [ "ksock_server_open", "kb__socket_8c.html#aeb825bdaacce848699d2334999014012", null ],
    [ "list_command", "kb__socket_8c.html#a89ac6441aca22dbf93fe15910eacec95", null ],
    [ "ksock_buf_len", "kb__socket_8c.html#a36313bc4c4efac4425f82cf6ff876822", null ],
    [ "ksock_buf_snd", "kb__socket_8c.html#a12ac96516b7753c24a51d9f31a91c1d2", null ],
    [ "ksock_cmd_list", "kb__socket_8c.html#a84ef2a3aae735da91865d08725a089d0", null ],
    [ "ksock_cmd_n", "kb__socket_8c.html#ac3d669239b73cbcb5d147af79c2a6df6", null ],
    [ "ksock_command_terminator", "kb__socket_8c.html#af6dcbfb876e19890b762a4fa823d7708", null ]
];