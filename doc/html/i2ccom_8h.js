var i2ccom_8h =
[
    [ "i2c_t", "structi2c__t.html", "structi2c__t" ],
    [ "i2c_dev_t", "i2ccom_8h.html#aa8978a980d106295ec6bcd5967c6efd8", null ],
    [ "i2c_close", "i2ccom_8h.html#aa00f0ec5de79f6c8535337e1dbe5b34f", null ],
    [ "i2c_exists", "i2ccom_8h.html#a1fef7a079c461a96208a89e2ee716037", null ],
    [ "i2c_llread", "i2ccom_8h.html#a23ca67af5334e15e71b4a3712031f0dd", null ],
    [ "i2c_lltransfer", "i2ccom_8h.html#a09a5dcd5737f6ffdb04a50c3f4e3c797", null ],
    [ "i2c_llwrite", "i2ccom_8h.html#a150258c37e58c033fc0af8b70e88821f", null ],
    [ "i2c_open", "i2ccom_8h.html#a6e740121c2a113454fe52e59f984a63d", null ],
    [ "i2c_scan", "i2ccom_8h.html#a4c8118b9e0cbe96062442c7d64fd2532", null ]
];